FROM ubuntu:22.04

RUN apt update

ENV DEBIAN_FRONTEND=noninteractive

RUN apt install texlive-latex-base biber texlive-fonts-recommended texlive-fonts-extra texlive-bibtex-extra -y

RUN apt install -y texlive-lang-italian texlive-latex-extra texlive-latex-recommended texlive-plain-generic texlive-xetex
