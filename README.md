# Latex in Docker

This repository provides a Docker image with all the packages needed to compile Latex files. This can be used to build pdf files directly with GitLab CI on GitLab for every commit done in your repository.

Simply copy the `example.gitlab-ci.yml` in your latex repository and name it `.gitlab-ci.yml`.

From that point on, for every commit you do in your repository, the pdf file will be generated.
